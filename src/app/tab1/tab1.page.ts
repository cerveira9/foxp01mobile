import { Component } from '@angular/core';
import { NoticiasService } from "../services/noticias.service";
import { Observable } from "rxjs";

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  noticias: Observable<any>;
  noticiasGet: Observable<any>;
  noticiasOld: Observable<any>;
  noticiasGetOld: Observable<any>;
  noticiasBuscadas: any;
  searchTerm: string = "";
  completo: any;

  constructor(private noticiasService: NoticiasService) {
    this.getData();
  }

  getData() {
    this.noticias = this.noticiasService.getDataNoticias();
    this.completo=true;
    this.noticias.subscribe(res => {
      this.noticiasGet = res;
      return res;
    });
  }

  searchChanged($event) {
    this.noticiasService
      .searchData(this.searchTerm)
      .subscribe(data => {
        this.noticiasBuscadas = data;
      });
  }

  loadMore($event) {
    this.noticiasOld = this.noticiasService.getDataNoticiasOld();

    this.noticiasOld.subscribe(res => {
      this.noticiasGetOld = res;
      this.completo=false;
      return res;
    });
  }

  doRefresh(event) {
    this.noticiasGet = null;
    this.getData();
    setTimeout(() => {
      event.target.complete();
    }, 100);
  }

}
