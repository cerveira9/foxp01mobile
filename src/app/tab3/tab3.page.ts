import { Component } from '@angular/core';
import { TabelaService } from "../services/tabela.service";
import { Observable } from "rxjs";

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  tabela: Observable<any>;
  tabelaGet: Observable<any>;

  constructor(private tabelaService: TabelaService) {
    this.getData();
  }

  getData() {
    this.tabela = this.tabelaService.getDataTabela();

  this.tabela.subscribe(res => {
      this.tabelaGet = res;
      return res;
    });
  }

  doRefresh(event) {
    this.tabelaGet = null;
    this.getData();
    setTimeout(() => {
      event.target.complete();
    }, 100);
  }

}
