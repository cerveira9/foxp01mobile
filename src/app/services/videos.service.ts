import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class VideosService {
  urlVideos = "http://191.252.111.143/rest/ws/getVideosMax10";

  urlVideosTeste = "http://localhost:8080/foxp01/rest/ws/getVideosMax10";

  // urlTeste = "https://jsonplaceholder.typicode.com/posts/42"

  constructor(private http: HttpClient) { }

  getDataVideos(): Observable<any> {
    return this.http.get(`${this.urlVideosTeste}/`).pipe(
      map(results => {
        return results;
      })
    );
  }

  getDetails(id) {
    return this.http.get(`${this.urlVideos}/${id}`);
  }
}
