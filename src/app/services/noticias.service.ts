import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

export enum SearchType {
  all = "",
  palavraChave = "palavraChave"
}

@Injectable({
  providedIn: 'root'
})
export class NoticiasService {

  urlNoticia = "http://191.252.111.143/rest/ws/getNoticiasMax10";

  urlNoticiaOld = "http://191.252.111.143/rest/ws/getNoticiasOld";

  urlNoticiaBusca = "http://191.252.111.143/rest/ws/buscarNoticia";

  urlNoticiaBuscaTeste = "http://localhost:8080/foxp01/rest/ws/buscarNoticia";

  urlNoticiaTeste = "http://localhost:8080/foxp01/rest/ws/getNoticiasMax10";

  urlNoticiaOldTeste = "http://localhost:8080/foxp01/rest/ws/getNoticiasOld";

  // urlTeste = "https://jsonplaceholder.typicode.com/posts/42"

  constructor(private http: HttpClient) { }

  getDataNoticias(): Observable<any> {
    return this.http.get(`${this.urlNoticiaTeste}/`).pipe(
      map(results => {
        return results;
      })
    );
  }

  getDataNoticiasOld(): Observable<any> {
    return this.http.get(`${this.urlNoticiaOldTeste}/`).pipe(
      map(results => {
        return results;
      })
    );
  }

  searchData(palavraChave: string) {
    return this.http.get(`${this.urlNoticiaBuscaTeste}/${palavraChave}`);
  }

  getDetails(id) {
    return this.http.get(`${this.urlNoticiaTeste}/${id}`);
  }
}
