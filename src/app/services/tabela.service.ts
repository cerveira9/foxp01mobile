import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class TabelaService {
  urlTabela = "http://191.252.111.143/rest/ws/getTabelaMax5";

  urlTabelaTeste = "http://localhost:8080/foxp01/rest/ws/getTabelaMax5";

  constructor(private http: HttpClient) { }

  getDataTabela(): Observable<any> {
    return this.http.get(`${this.urlTabela}/`).pipe(
      map(results => {
        return results;
      })
    );
  }

  getDetails(id) {
    return this.http.get(`${this.urlTabela}/${id}`);
  }
}
