import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NoticiasService } from "../services/noticias.service";

@Component({
  selector: 'app-detalhes-noticias',
  templateUrl: './detalhes-noticias.page.html',
  styleUrls: ['./detalhes-noticias.page.scss'],
})
export class DetalhesNoticiasPage implements OnInit {
  detail = null;

  constructor(private activatedRoute: ActivatedRoute, private noticiasService: NoticiasService) { }

  ngOnInit() {
    let id = this.activatedRoute.snapshot.paramMap.get('id');

    this.noticiasService.getDetails(id).subscribe(result => {
      this.detail = result;
    })
  }

}
