import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetalhesNoticiasPage } from './detalhes-noticias.page';

const routes: Routes = [
  {
    path: '',
    component: DetalhesNoticiasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetalhesNoticiasPageRoutingModule {}
