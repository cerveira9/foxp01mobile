import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetalhesNoticiasPageRoutingModule } from './detalhes-noticias-routing.module';

import { DetalhesNoticiasPage } from './detalhes-noticias.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetalhesNoticiasPageRoutingModule
  ],
  declarations: [DetalhesNoticiasPage]
})
export class DetalhesNoticiasPageModule {}
