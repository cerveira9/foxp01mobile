import { Component } from '@angular/core';
import { VideosService } from "../services/videos.service";
import { Observable } from "rxjs";

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  videos: Observable<any>;
  videosGet: Observable<any>;

  constructor(private videosService: VideosService) {
    this.getData();
  }

  getData() { 
    this.videos = this.videosService.getDataVideos();

  this.videos.subscribe(res => {
      this.videosGet = res;
      return res;
    });
  }

  doRefresh(event) {
    this.videosGet = null;
    this.getData();
    setTimeout(() => {
      event.target.complete();
    }, 100);
  }

}
